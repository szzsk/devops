FROM .com/base/cmrhjava:8

COPY target/provider-1.0.0-SNAPSHOT.jar /home

RUN localedef -c -f UTF-8 -i zh_CN zh_CN.utf8
ENV LC_ALL zh_CN.UTF-8